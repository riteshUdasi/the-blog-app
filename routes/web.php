<?php

use App\Http\Controllers\CatogoriesController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\TagsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [FrontendController::class, 'index']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

//APPLICATION ROUTES
//Note: have used raw url instead of categories.destroy
Route::resource('categories', CatogoriesController::class)->middleware(['auth']);
Route::resource('tags', TagsController::class)->middleware(['auth']);
