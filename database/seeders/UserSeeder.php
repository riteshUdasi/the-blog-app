<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ritesh Udasi',
            'email' => 'udasiritesh@gmail.com',
            'password' => Hash::make('abcd1234')
        ]);
        User::create([
            'name' => 'John Doe',
            'email' => 'doefam1@gmail.com',
            'password' => Hash::make('abcde12345')
        ]);
        User::create([
            'name' => 'Jane Doe',
            'email' => 'doefam2@gmail.com',
            'password' => Hash::make('abcdefgh12345678')
        ]);
        User::create([
            'name' => 'Jelly Doe',
            'email' => 'doefam3@gmail.com',
            'password' => Hash::make('abcdefg1234567')
        ]);
        User::create([
            'name' => 'Jimmy Doe',
            'email' => 'doefam4@gmail.com',
            'password' => Hash::make('abcdef123456')
        ]);
    }
}
