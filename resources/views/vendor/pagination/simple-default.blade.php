@if ($paginator->hasPages())
    <!-- Blog Paging
    ===================================== -->
    <div class="row mt25 animated" data-animation="fadeInUp" data-animation-delay="100">
        @if ($paginator->onFirstPage())
            <div class="col-md-6">
                <a href="javascript:void(0);" class="button button-sm button-pasific pull-left" style="cursor:not-allowed; background-color:gray !important" >
                    Old Entries
                </a>
            </div>
        @else
            <div class="col-md-6">
                <a href="{{ $paginator->previousPageUrl() }}" class="button button-sm button-pasific pull-left hover-skew-backward">
                    Old Entries
                </a>
            </div>
        @endif

        @if ($paginator->hasMorePages())
            <div class="col-md-6">
                <a href="{{ $paginator->nextPageUrl() }}" class="button button-sm button-pasific pull-right hover-skew-forward">New Entries</a>
            </div>
        @else
            <div class="col-md-6">
                <a href="javascript:void(0);" class="button button-sm button-pasific pull-right" style="cursor:not-allowed; background-color:gray !important">New Entries</a>
            </div>
        @endif
    </div>
@endif
